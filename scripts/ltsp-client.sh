#!/bin/bash
### start/stop a VirtualBox LTSP client

usage() {
    echo "
Usage:
    $0 [-x] start [<client-name>] [<options>]
    $0 [-x] stop [<client-name>]
    $0 [-h|--help]

Start a VirtualBox machine that has no disk and boots from network.

<client-name>
    The name of the client. Default value 'ltsp-client'.

Options:

-i,--lan-if <LAN-interface>
    The interface that is connected to the LAN. Default 'ltsptest01'.

-m,--memory <RAM-in-MB>
    The size of the RAM. Default 1024.

Option '-x' can be used to enable debugging.
"
    exit
}


cmd_start() {
    # stop it first, if it is started
    cmd_stop &>/dev/null

    # get the options
    local lan_if='ltsptest01'
    local memory='1024'
    local opts="$(getopt -o i:m: -l lan-if:,memory: -n "$0" -- "$@")" || usage
    eval set -- "$opts"
    while true; do
        case $1 in
            -i|--lan-if)
                lan_if=$2; shift 2 ;;
            -m|--memory)
                memory=$2; shift 2 ;;
            --)
                shift; break ;;
        esac
    done

    # get the client name
    local client_name=${1:-ltsp-client}

    # check the LAN interface lan_if
    local interfaces="$(ip link | grep -v link | cut -d: -f2 | grep -v lo | xargs echo)"
    if [[ -z $(echo $interfaces | grep -s $lan_if) ]]; then
        echo -e "\nError: '$lan_if' is not a valid interface. It should be one of:\n--> $interfaces\n"
        exit 1
    fi

    # create a new virtual machine and set its parameters
    VBoxManage createvm --name $client_name --register
    VBoxManage modifyvm $client_name \
               --memory $memory \
               --acpi on \
               --boot1 net \
               --nic1 bridged \
               --bridgeadapter1 $lan_if

    # start the virtual machine
    VBoxManage startvm $client_name
}

cmd_stop() {
    # get the client name
    local client_name=${1:-ltsp-client}

    VBoxManage controlvm $client_name poweroff
    sleep 2
    VBoxManage unregistervm $client_name --delete
}

main() {
    local cmd=$1; shift
    case $cmd in
        start)        cmd_start "$@" ;;
        stop)         cmd_stop "$@" ;;
        -h|--help|*)  usage ;;
    esac
}

# enable debugging
[[ $1 == '-x' ]] && set -x && shift

# start the main function
main "$@"
