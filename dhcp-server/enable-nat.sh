#!/bin/bash -x

# enable ip forwarding
echo 1 > /proc/sys/net/ipv4/ip_forward
sed -i /etc/sysctl.conf \
    -e '/net.ipv4.ip_forward/d'
echo 'net.ipv4.ip_forward=1' >> /etc/sysctl.conf

# delete any existing iptables rules
iptables --flush
iptables --table nat --flush
iptables --delete-chain
iptables --table nat --delete-chain

# add IP masquerading rules
wan_if=${WAN:-enp0s3}
lan_if=${LAN:-enp0s8}
iptables -t nat -A POSTROUTING -o $wan_if -j MASQUERADE
iptables -A FORWARD -i $wan_if -o $lan_if -m state --state RELATED,ESTABLISHED -j ACCEPT
iptables -A FORWARD -i $lan_if -o $wan_if -j ACCEPT

# install iptables-persistent and save iptables rules
apt install --yes iptables-persistent
iptables-save > /etc/iptables/rules.v4
