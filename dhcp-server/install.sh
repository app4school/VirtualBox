#/bin/bash -x

export DEBIAN_FRONTEND=noninteractive

apt update
apt upgrade --yes

/host/dhcp-server/dnsmasq.sh
/host/dhcp-server/setup-networking.sh
/host/dhcp-server/enable-nat.sh    # make it a gateway
/host/dhcp-server/mount-shared-folders.sh
