#!/bin/bash -x
### This is a hook script that runs before 'install.sh'.
### Feel free to change and customize it if needed.
### Run 'git update-index --skip-worktree ltsp-server/pre-install.sh'
### so that git ignores the changes to this file.

include settings.sh

# install additinal packages
apt update
apt upgrade --yes
apt install --yes --install-recommends \
    xfce4-terminal

# set US keyboard layout (see `man keyboard`
sed -i /etc/default/keyboard \
    -e '/XKBLAYOUT/ c XKBLAYOUT="us"'

# set default locale
#sed -i /etc/locale.gen \
#    -e '/en_US.UTF-8/ c en_US.UTF-8 UTF-8'
#locale-gen
update-locale --reset \
              LANG="en_US.UTF-8" \
              LANGUAGE="en_US:en"

