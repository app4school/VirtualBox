#!/bin/bash

# enable debugging
[[ $1 == '-x' ]] && set -x && shift

# load settings (LAN_IF, GATEWAY, VM_RAM etc.)
source settings.sh
DUMMY_IF='ltsptest01'

fail() { echo "$@" >&2; exit 1; }

usage() {
    fail "
Usage: $0 [-x] <command> [<options>]

Manage the virtual machine of the ltsp-server.

When testing (DEVELOPMENT='yes' in settings.sh), then the virtual
machine of the ltsp-client will be managed as well.

When testing the normal mode (DEVELOPMENT='yes' and GATEWAY='no'),
then the virtual machine of the dhcp-server will be managed as well.

When testing internally (DEVELOPMENT='yes' and LAN_IF='ltsptest01'),
then a dummy network interface named 'ltsptest01' will be created as well.

Commands:

    build
        Start up and install the virtual machine(s).

    destroy
        Destroy the virtual machine(s).

    up | halt | suspend | resume | reload | provision | status
        Pass these commands to 'vagrant'.

    reboot
        Shutdown and restart.

Option '-x' is for debugging.
"
}

testing() {
    [[ ${DEVELOPMENT,,} == "yes" ]]
}

testing_normal_mode() {
    testing && [[ ${GATEWAY,,} != "yes" ]]
}

dummy_interface() {
    [[ $LAN_IF == $DUMMY_IF ]]
}
internal_testing() {
    testing && dummy_interface
}

vagrant_start() {
    if testing_normal_mode; then
        cd dhcp-server/
        vagrant "$@"
        cd ..
    fi

    cd ltsp-server/
    vagrant "$@"
    cd ..
}

vagrant_stop() {
    cd ltsp-server/
    vagrant "$@"
    cd ..

    if testing_normal_mode; then
        cd dhcp-server/
        vagrant "$@"
        cd ..
    fi
}

there_is_a_dummy_interface() {
    ip link show $DUMMY_IF &>/dev/null
}

create_dummy_interface() {
    there_is_a_dummy_interface && destroy_dummy_interface
    sudo modprobe dummy
    sudo ip link add $DUMMY_IF type dummy
    sudo ip link set $DUMMY_IF up
}

destroy_dummy_interface() {
    there_is_a_dummy_interface || return
    sudo ip link delete $DUMMY_IF type dummy
    sudo rmmod dummy
}

check_settings() {
    if ! testing; then
        dummy_interface && fail "Error: Edit 'settings.sh' and set a propper value for LAN_IF."
        [[ $HOSTNAME == 'ltsp-server' ]] \
	            && fail "Error: Edit 'settings.sh' and set a propper value for HOSTNAME."
        [[ $ADMIN_PASS == 'pass' ]] \
	            && fail "Error: ADMIN_PASS on 'settings.sh' has to be changed for security reasons."
        [[ $GUEST_PASS == 'pass' ]] \
	            && fail "Error: GUEST_PASS on 'settings.sh' has to be changed for security reasons."
        [[ $GUAC_PASS == 'pass' ]] \
	            && fail "Error: GUAC_PASS on 'settings.sh' has to be changed for security reasons."
    fi

    # check the LAN interface
    if ! dummy_interface; then
        local interfaces="$(ip link | grep -v link | cut -d: -f2 | grep -v lo | xargs echo)"
        echo $interfaces | grep $LAN_IF &>/dev/null \
	    || fail "
Error: LAN_IF=\"$LAN_IF\" is not a valid interface. It should be one of:
--> $interfaces
Edit 'settings.sh' and fix it.
"
    fi
}

main() {
    internal_testing && create_dummy_interface
    local cmd=$1
    case $cmd in
        build)
            check_settings
            # install the server
            vagrant_stop destroy -f 2>/dev/null
            vagrant_start up
            # restart
            vagrant_stop halt
            vagrant_start up
            # if testing, start the client as well
            testing && scripts/ltsp-client.sh start --lan-if $LAN_IF --memory $VM_RAM
            ;;
        destroy)
            testing && scripts/ltsp-client.sh stop 2>/dev/null
            vagrant_stop destroy -f
            internal_testing && destroy_dummy_interface
            ;;
        up|resume|reload|provision|status)
            vagrant_start "$@"
            ;;
        halt|suspend)
            vagrant_stop "$@"
            ;;
        reboot)
            vagrant_stop halt 2>/dev/null
            vagrant_start up
            ;;
        *)
            usage
            ;;
    esac
}

internal_testing && [[ -z $(which sudo) ]] && fail "
Please install sudo first. For example see this tutorial:
https://www.vultr.com/docs/how-to-use-sudo-on-debian-centos-and-freebsd
"

# start the main function
cd $(dirname $0)
mkdir -p logs/
logfile="logs/ltsp-server-$(date +%Y%m%d).log"
cat <<EOF | tee --append $logfile


============================================================
==> $(date)  $0 $*
============================================================

EOF
main "$@" 2>&1 | tee --append $logfile
